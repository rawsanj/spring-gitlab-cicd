package com.rawsanj.cicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by @author Sanjay Rawat on 4/3/17.
 */

@SpringBootApplication
public class SpringGitlabCicdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringGitlabCicdApplication.class, args);
	}

}
