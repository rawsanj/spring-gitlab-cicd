package com.rawsanj.cicd.repository;

import com.rawsanj.cicd.domain.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by @author Sanjay Rawat on 4/3/17.
 */

@RepositoryRestResource
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
}
