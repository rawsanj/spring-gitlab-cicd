package com.rawsanj.cicd.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;

/**
 * Created by @author Sanjay Rawat on 4/3/17.
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Blog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @URL(message = "Invalid URL!")
    private String url;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

}
