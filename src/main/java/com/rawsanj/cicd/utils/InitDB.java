package com.rawsanj.cicd.utils;

import com.rawsanj.cicd.domain.Blog;
import com.rawsanj.cicd.domain.User;
import com.rawsanj.cicd.repository.BlogRepository;
import com.rawsanj.cicd.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

/**
 * Created by @author Sanjay Rawat on 4/3/17.
 */

@Configuration
public class InitDB {

    private final Logger logger = LoggerFactory.getLogger(InitDB.class);

    private UserRepository userRepository;

    private BlogRepository blogRepository;

    @Autowired
    public InitDB(UserRepository userRepository, BlogRepository blogRepository) {
        this.userRepository = userRepository;
        this.blogRepository = blogRepository;
    }

    @Bean
    CommandLineRunner commandLineRunner(){
        return strings -> {

            logger.info("Initializing Database!");

            User user = new User(null, "Sanjay", "sanjay@gmail.com", null);

            User userInDB = userRepository.save(user);

            Blog javaBlog = new Blog(null, "Java", "https://dzone.com/java-jdk-development-tutorials-tools-news", userInDB);
            Blog devopsBlog = new Blog(null, "DevOps", "https://dzone.com/devops-tutorials-tools-news", userInDB);
            Blog cloudBlogs = new Blog(null, "Cloud Computing", "https://dzone.com/cloud-computing-tutorials-tools-news", userInDB);

            List<Blog> blogs = Arrays.asList(javaBlog, devopsBlog, cloudBlogs);
            user.setBlogs(blogs);

            blogRepository.save(blogs);

            logger.info("Number of Users in DB: {}", userRepository.count());

            logger.info("Number of Blogs in DB: {}", blogRepository.count());

            userRepository.findAll().forEach(u -> {
                logger.info("User in DB: {}", u.getName());
            });

            blogRepository.findAll().forEach(b->{
                logger.info("Blog in DB: {}", b.getName());
            });

        };
    }
}
