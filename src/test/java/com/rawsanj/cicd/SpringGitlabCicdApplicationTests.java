package com.rawsanj.cicd;

import com.rawsanj.cicd.domain.Blog;
import com.rawsanj.cicd.domain.User;
import com.rawsanj.cicd.repository.BlogRepository;
import com.rawsanj.cicd.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class SpringGitlabCicdApplicationTests {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BlogRepository blogRepository;

	@Autowired
	private TestRestTemplate testRestTemplate;

	@Before
	public void deleteData() {

		logger.info("Deleting all data created during Init");

		userRepository.findAll().forEach(user -> {
			testRestTemplate.delete("/users/"+ user.getId());
		});

	}

	@Test
	public void createUserAndBlogWithRepository(){

		User user = new User(null, "Sanjay", "sanjay@gmail.com", null);

		User userInDB = userRepository.save(user);

		Blog javaBlog = new Blog(null, "Java", "https://dzone.com/java-jdk-development-tutorials-tools-news", userInDB);
		Blog devopsBlog = new Blog(null, "DevOps", "https://dzone.com/devops-tutorials-tools-news", userInDB);
		Blog cloudBlogs = new Blog(null, "Cloud Computing", "https://dzone.com/cloud-computing-tutorials-tools-news", userInDB);

		List<Blog> blogs = Arrays.asList(javaBlog, devopsBlog, cloudBlogs);
		user.setBlogs(blogs);

		blogRepository.save(blogs);

		Long userCount = userRepository.count();
		Long blogCount = blogRepository.count();

		assertThat(userCount).isEqualTo(1L);
		assertThat(blogCount).isEqualTo(3L);

	}

	@Test
	public void createUserWithRestEndpoint(){

		User user = new User(null, "Sanjay", "sanjay@gmail.com", null);
		ResponseEntity<User> response = testRestTemplate.postForEntity("/users", user, User.class);

		assertThat(Integer.parseInt(response.getStatusCode().toString())).isEqualTo(201);
	}

	@Test
	public void createBlogWithRestEndpoint(){

		Blog blog = new Blog(null, "Java", "http://www.java.com", null);
		ResponseEntity<Blog> response = testRestTemplate.postForEntity("/users", blog, Blog.class);

		assertThat(Integer.parseInt(response.getStatusCode().toString())).isEqualTo(201);
	}





}
