//import _root_.io.gatling.core.scenario.Simulation
//import ch.qos.logback.classic.{Level, LoggerContext}
//import io.gatling.core.Predef._
//import io.gatling.http.Predef._
//import org.slf4j.LoggerFactory
//
//import scala.concurrent.duration._
//
///**
// * Performance test for the websites
// */
//class SiteGatlingTest extends Simulation {
//
//    val context: LoggerContext = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
//    // Log all HTTP requests
//    //context.getLogger("io.gatling.http").setLevel(Level.valueOf("TRACE"))
//    // Log failed HTTP requests
//    //context.getLogger("io.gatling.http").setLevel(Level.valueOf("DEBUG"))
//
//    val baseURL = Option(System.getProperty("baseURL")) getOrElse """http://www.example.com/rest/end-point"""
//
//    val httpConf = http
//        .baseURL(baseURL)
//        .inferHtmlResources()
//        .acceptHeader("*/*")
//        .acceptEncodingHeader("gzip, deflate")
//        .acceptLanguageHeader("fr,fr-fr;q=0.8,en-us;q=0.5,en;q=0.3")
//        .connectionHeader("keep-alive")
//        .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:33.0) Gecko/20100101 Firefox/33.0")
//
//    val headers_http = Map(
//        "Accept" -> """application/json"""
//    )
//
//    val headers_http_authenticated = Map(
//        "Accept" -> """application/json"""
//    )
//
//    val scn = scenario("Test Website")
//            .repeat(5) {
//                exec(http("Get website")
//                .get("/register.json")
//                .headers(headers_http_authenticated)
//                .check(status.is(200)))
//                .pause(10)
//            }
//
//    val website = scenario("website").exec(scn)
//
//    setUp(
//        website.inject(rampUsers(5000) over (1 minutes))
//    ).protocols(httpConf)
//    .assertions(
//        global.responseTime.max.lte(200000)
//    )
//}
